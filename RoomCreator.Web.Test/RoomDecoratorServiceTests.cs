using System;
using Xunit;
using RoomCreator.Web.Data;

namespace RoomCreator.Web.Test
{
    public class RoomDecoratorTests
    {
        [Theory]
        [InlineData(5,10,2,4,false,50,100,15)]
        [InlineData(5,10,2,4,true,50,100,27.5)]
        public async void TestRoomDecoratorReturnsExpectedValues(double length, double width, double height, double paintCoverage, bool includeCeiling,
            double expectedFloorArea, double expectedVolume, double expectedPaintRequired)
        {
            RoomDecoratorService service = new RoomDecoratorService();
            var request = new RoomDecoratorRequest{
                Length = length,
                Width = width,
                Height = height,
                PaintCoverage = paintCoverage,
                IncludeCeiling = includeCeiling
            };
            var result = await service.GetRoomDetails(request);
            Assert.NotNull(result.Room);
            Assert.Equal(expectedFloorArea, result.Room.FloorArea);
            Assert.Equal(expectedVolume, result.Room.Volume);
            Assert.Equal(expectedPaintRequired, result.PaintRequired);
        }
    }
}
