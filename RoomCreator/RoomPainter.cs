using System;
using RoomCreator.Interfaces;

namespace RoomCreator{
    public class RoomPainter : IRoomPainter
    {
        public double GetRequiredPaint(IRoom room, IPaint paint, bool includeCeiling = false)
        {
            if (room == null)
                throw new ArgumentNullException("Room cannot be null");
            if (paint.Coverage <= 0)
                throw new ArgumentException("Paint coverage must be greater than or equal to zero");
            double ceilingArea = 0;
            if (includeCeiling)
                ceilingArea = room.CeilingArea;
            return (room.TotalWallArea + ceilingArea) / paint.Coverage;
        }
    }
}