﻿using System;
using RoomCreator.Interfaces;

namespace RoomCreator
{
    public class Room : IRoom
    {
        public double Length{ get; private set; }
        public double Width{ get; private set; }
        public double Height{ get; private set; }
        public Room(double length, double width, double height)
        {
            if (width <= 0)
                throw new ArgumentException("Width must be greater than 0");
            if (height <= 0)
                throw new ArgumentException("Height must be greater than 0");
            if (length <= 0)
                throw new ArgumentException("Length must be greater than 0");
            Length = length;
            Width = width;
            Height = height;
        }

        public double FloorArea => Length * Width;
        // This is done to accomodate other types of rooms i.e those with a dome or pitched roof later but in this basic room example
        // the area of the ceiling is the same as the area of the floor.
        public double CeilingArea => FloorArea;

        public double Volume => Length * Width * Height;

        public double TotalWallArea => (LengthWallArea + WidthWallArea) * 2;
        private double WidthWallArea => Width * Height;
        private double LengthWallArea => Length * Height;
    }
}
