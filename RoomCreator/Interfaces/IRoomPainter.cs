using RoomCreator.Interfaces;

public interface IRoomPainter
{
    double GetRequiredPaint(IRoom room, IPaint paint, bool includeCeiling = false);
}