namespace RoomCreator.Interfaces{
    public interface IPaint
    {
        double Coverage { get; set; }
    }
}