using System.Collections.Generic;

namespace RoomCreator.Interfaces
{
    public interface IRoom
    {
        double FloorArea { get; }
        double Volume { get; }
        double TotalWallArea { get; }
        double CeilingArea{ get; }
    }
}
