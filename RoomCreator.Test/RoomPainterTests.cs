using System;
using RoomCreator.Interfaces;
using Xunit;

namespace RoomCreator.Test{
    public class RoomPainterTests{
        [Fact]
        public void RoomPainterThrowsExceptionWithNullRoom(){
            IRoomPainter roomPainter = new RoomPainter();
            IRoom room = null;
            IPaint paint = new Paint{Coverage=20.5};
            Assert.Throws<ArgumentNullException>(() => roomPainter.GetRequiredPaint(room, paint));
        }

        [Fact]
        public void RoomPainterThrowsExceptionWithNegativePaintCoverage(){
            IRoomPainter roomPainter = new RoomPainter();
            IRoom room = new Room(10,10,10);
            IPaint paint = new Paint{Coverage=-7.5};
            Assert.Throws<ArgumentException>(() => roomPainter.GetRequiredPaint(room, paint));
        }

        [Fact]
        public void RoomPainterThrowsExceptionWithZeroPaintCoverage(){
            IRoomPainter roomPainter = new RoomPainter();
            IRoom room = new Room(10,10,10);
            IPaint paint = new Paint{Coverage=0};
            Assert.Throws<ArgumentException>(() => roomPainter.GetRequiredPaint(room, paint));
        }

        [Theory]
        [InlineData(10, 10, 10, 5, 80)]
        [InlineData(10,10,5, 12.1,16.52893)]
        [InlineData(10, 5, 4, 7.25, 16.55172)]
        [InlineData(7.5, 15.75, 2.25, 11.5, 9.09783)]
        public void RoomPainterCalculatesPaintCoverageWithoutCeiling(double length, double width, double height, double paintCoverage, double expectedPaintVolume){
            IRoomPainter roomPainter = new RoomPainter();
            IRoom room = new Room(length,width,height);
            IPaint paint = new Paint{Coverage=paintCoverage};
            Assert.Equal(Math.Round(expectedPaintVolume, 5), Math.Round(roomPainter.GetRequiredPaint(room, paint), 5));
        }

        [Theory]
        [InlineData(10, 10, 10, 5, 100)]
        [InlineData(10,10,5, 12.1,24.79339)]
        [InlineData(10, 5, 4, 7.25, 23.44828)]
        [InlineData(7.5, 15.75, 2.25, 11.5, 19.36957)]
        public void RoomPainterCalculatesPaintCoverageWithCeiling(double length, double width, double height, double paintCoverage, double expectedPaintVolume){
            IRoomPainter roomPainter = new RoomPainter();
            IRoom room = new Room(length,width,height);
            IPaint paint = new Paint{Coverage=paintCoverage};
            Assert.Equal(Math.Round(expectedPaintVolume, 5), Math.Round(roomPainter.GetRequiredPaint(room, paint, true), 5));
        }
    }
}