using System;
using System.Threading.Tasks;
using RoomCreator.Interfaces;
using Xunit;

namespace RoomCreator.Test
{
    public class RoomTests
    {
        [Fact]
        public void CreatingARoomWithNegativeWidthThrowsAnException()
        {
            Assert.Throws<ArgumentException>(() => new Room(100, -10, 25));
        }
        
        [Fact]
        public void CreatingARoomWithNegativeLengthThrowsAnException(){
            Assert.Throws<ArgumentException>(() => new Room(-100, 10, 25));
        }

        [Fact]
        public void CreatingARoomWithNegativeHeightThrowsAnException(){
            Assert.Throws<ArgumentException>(() => new Room(100, 10, -25));
        }

        [Theory]
        [InlineData(10,10,100)]
        [InlineData(5,5,25)]
        [InlineData(10,40,400)]
        [InlineData(5.5,5.5,30.25)]
        [InlineData(10.1, 40.4, 408.04)]
        public void FloorAreaTest(double length, double width, double expectedArea){
            IRoom room = new Room(length, width, 1);
            Assert.Equal(Math.Round(expectedArea, 5), Math.Round(room.FloorArea, 5));
        }

        [Theory]
        [InlineData(10,10,10,1000)]
        [InlineData(10,10,5, 500)]
        [InlineData(10, 5, 4, 200)]
        [InlineData(7.5, 15.75, 2.25, 265.78125)]
        public void RoomVolumeTest(double length, double width, double height, double expectedVolume){
            IRoom room = new Room(length, width, height);
            Assert.Equal(Math.Round(expectedVolume, 5), Math.Round(room.Volume, 5));
        }
    }
}