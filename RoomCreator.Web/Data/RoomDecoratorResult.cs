using RoomCreator.Interfaces;

namespace RoomCreator.Web.Data{
    public class RoomDecoratorResult{
        public double PaintRequired{get;set;}
        public IRoom Room{ get; set; }
    }
}