using System.Threading.Tasks;
using RoomCreator.Interfaces;

namespace RoomCreator.Web.Data{
    public class RoomDecoratorService{
        public Task<RoomDecoratorResult> GetRoomDetails(RoomDecoratorRequest request){
            return Task.Run(() => { 
                IRoomPainter roomPainter = new RoomPainter();
                IRoom room = new Room(request.Length, request.Width, request.Height);
                IPaint paint = new Paint { Coverage=request.PaintCoverage };
                return new RoomDecoratorResult(){
                    Room = room,
                    PaintRequired = roomPainter.GetRequiredPaint(room, paint, request.IncludeCeiling)
                }; 
            });
        }
    }
}