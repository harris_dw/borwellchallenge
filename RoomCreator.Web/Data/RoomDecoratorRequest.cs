using System.ComponentModel.DataAnnotations;

namespace RoomCreator.Web.Data{
    public class RoomDecoratorRequest{
        const double _minValue = 0.01;
        const double _maxSizeValue = 5000.0;
        const double _maxCoverage = 100.00;
        [Required]
        [Range(_minValue, _maxSizeValue)]
        public double Length { get; set; }
        [Required]
        [Range(_minValue, _maxSizeValue)]
        public double Width { get; set; }
        [Required]
        [Range(_minValue, _maxSizeValue)]
        public double Height { get; set; }
        [Required]
        [Range(_minValue, _maxCoverage)]
        public double PaintCoverage { get; set; }
        public bool IncludeCeiling { get; set; }
    }
}