# Room Creator
## How to Run - Web Project
1. Run the run.cmd file that is in the root of the RoomDecorator.Web folder
2. To stop the server ctrl+c will prompt you whether you want to shut the server down.

## How to Run - Unit Tests
1. Open the windows command prompt
2. Navigate to the RoomCreator.Test directory
3. Run the command `dotnet test`

## Viewing the Code
The Borwell Challenge folder can be opened with Visual Studio Code.

## Design and Further development considerations
- The code only handles square and rectangular rooms at the moment but it is built in a way that different shaped rooms that implement the IRoom interface will allow the system to be extended to different shaped rooms.
- The code at the moment assumes that all inputs are in meters. Area results are assumed to be in square and volume is returned as cubic meters. Paint coverage is expected to be square meters per litre and the returned value is in litres. To extend the system it would be beneficial if the user could specify the unit that values are being submitted in and request a unit in which values are to be returned. I think this is outside of the scope of this challenge though.
- It is often necessary to apply multiple coats of paint to get even coverage and it would be beneficial to add the number of coats required to the system to provide actual coverage.
- I have added some basic validation to the system to prevent erroneous values being put through but there has been no upper limits added to any of the values some sensible constraints may be required to prevent misuse.
- I have carried out rounding to 5 decimal places in the tests as there is an inaccuracy in using the double data type but I think that 5 decimal places is a sufficient level of accuracy for a system such as this. If further precision is requried this could be changed out for the decimal data type.
- Need to look into how to better unit test the blazor components.